require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest
  test "invalid signup information" do
    get new_user_path
    assert_no_difference 'User.count' do
      post users_path, params: { user: { name: "", email: "linda@invalid", password: "foo", password_confirmation: "bar" } }
      end
    assert_template 'users/new'
  end

  test "valid signup information" do
  get new_user_path
  assert_difference 'User.count', 1 do
    post users_path, params: { user: { name: "linda", email: "linda@example.com", password: "password", password_confirmation: "password" } }
  end
  follow_redirect!
  assert_template 'users/show'
  delete logout_path
  assert_not is_logged_in?
  assert_redirected_to login_path
  follow_redirect!
  end
end
